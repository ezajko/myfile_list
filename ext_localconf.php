<?php
defined('TYPO3_MODE') || die();

$boot = function () {
    /* ===========================================================================
        Extbase-based plugin
    =========================================================================== */

    /* ===========================================================================
        Register default template layouts
    =========================================================================== */
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['file_list']['templateLayouts'][] = [
        'LLL:EXT:myfile_list/Resources/Private/Language/locallang_flexform.xlf:filelist.templateLayout.supersimple',
        'Supersimple',
    ];


};

$boot();
unset($boot);
