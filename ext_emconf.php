<?php

########################################################################
# Extension Manager/Repository config file for ext: "myfile_list"
#
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Customization of file_list extension',
	'description' => 'This extension adapts the presentaion of files.',
	'version' => '10.4.0',
	'shy' => 0,
	'dependencies' => 'file_list',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Karel Kuijpers',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
	            'typo3' => '8.7.13-10.4.99',
                    'file_list' => '2.2.2-',
	        ),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:16:{s:12:"ext_icon.gif";s:4:"cb15";s:17:"ext_localconf.php";s:4:"1b44";s:14:"ext_tables.php";s:4:"da4b";s:28:"ext_typoscript_editorcfg.txt";s:4:"fbad";s:24:"ext_typoscript_setup.txt";s:4:"ac17";s:13:"locallang.php";s:4:"0bf8";s:16:"locallang_db.php";s:4:"5d22";s:14:"doc/manual.sxw";s:4:"fef6";s:14:"pi1/ce_wiz.gif";s:4:"dd56";s:36:"pi1/class.tx_macinasearchbox_pi1.php";s:4:"4957";s:44:"pi1/class.tx_macinasearchbox_pi1_wizicon.php";s:4:"8e35";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.php";s:4:"6962";s:12:"pi1/lupe.gif";s:4:"1fe9";s:20:"pi1/teaserheader.gif";s:4:"b6be";s:16:"pi1/template.htm";s:4:"4a0b";}',
);

?>